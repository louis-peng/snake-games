package com.candy.snake;

import javax.swing.*;

/**
 * @author 
 * @date 2022-02-25 15:15:22
 */
public class Start {
    public static void main(String[] args) {
        // 1、绘制一个静态窗口 JFrame
        JFrame jFrame = new JFrame("贪吃蛇小游戏");
        // 设置界面的位置和大小
        jFrame.setBounds(500,200,900,720);
        // 窗口大小不可变
        jFrame.setResizable(false);
        // 关闭窗口事件
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // 2、添加画板
        jFrame.add(new GamePanel());

        // 3、展现窗口
        jFrame.setVisible(true);
    }
}
